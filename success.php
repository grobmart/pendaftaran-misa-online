<?php
session_start();
//load librari QR Code
require("phpqrcode/qrlib.php"); //credit to https://github.com/t0k4rt/phpqrcode
require("config.php");
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Eldwen - Byxel.net">
        <title>QR Code Pendaftaran Misa</title>
        <!-- Bootstrap core CSS -->
        <link href="assets/dist/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="form-validation.css" rel="stylesheet">
    </head>
    <body class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <?php
                    if (isset($_SESSION['token']) && isset($_GET['x'])) {
                        //echo $_GET['x'];
                        //ga di design ya, mepet
                        //generate file name
                        $rand = substr(md5(uniqid(rand(), true)), 0, 3);
                        $file = date('YmdHis', time());
                        $newfilename = $file . $rand . '.png';

                        //create qrcode
                        QRcode::png(WEB_SERVER."qreader.php?token=".$_GET['x'], 'tmpqr/' . $newfilename, "H", 10, 5);
                        echo '<center>';
                        echo "<img src='tmpqr/$newfilename'/ style='max-width:100%;'><br /><br />";
                        echo "<a href='tmpqr/$newfilename' download class='btn btn-primary btn-lg'>Download QR Code</a>";
                        echo '<br /><br /><h3>Download atau screenshot dan tunjukan QR Code ini pada petugas yang menjaga di Gereja untuk bisa mengikuti misa</h3>';
                        echo '</center>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
session_unset();
session_destroy();
?>