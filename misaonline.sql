-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2020 at 12:43 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `misaonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `checkin`
--

CREATE TABLE `checkin` (
  `checkin_id` int(11) NOT NULL,
  `jadwal_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `telepon` varchar(16) NOT NULL,
  `alamat` text NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `usia_id` int(11) NOT NULL,
  `asal_paroki` enum('DALAM','LUAR') NOT NULL,
  `paroki_lainnya` varchar(160) NOT NULL,
  `asal_lingkungan` int(11) NOT NULL DEFAULT 0,
  `asal_keuskupan` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `token` text NOT NULL,
  `code` text NOT NULL,
  `status` enum('CHECKED_IN','ABSENT','BLOCKED','ACTIVE') NOT NULL COMMENT '''CHECKED_IN => Sudah Masuk Gereja'',''ABSENT => Tidak Hadir'',''BLOCKED => Blacklist'',''ACTIVE => Baru Daftar Saja, Belum Checkin''',
  `ip_address` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `checkin`
--

INSERT INTO `checkin` (`checkin_id`, `jadwal_id`, `nama`, `telepon`, `alamat`, `gender`, `usia_id`, `asal_paroki`, `paroki_lainnya`, `asal_lingkungan`, `asal_keuskupan`, `date_added`, `token`, `code`, `status`, `ip_address`) VALUES
(1, 1, 'ASDASDA', '23123', 'ASDASD', 'M', 2, 'DALAM', '', 3, '', '2020-07-07 16:20:19', '43ad6789387acff5d6225da11b2772ffa424c5a2ca532525ccf2f39040dd30104388aa2797941a0475da75992a8fb22d472270b0dbe26262e4cb48d62c2bae93', '860', 'ACTIVE', '::1'),
(2, 2, 'ASDASDA', '23123', 'ASDASD', 'M', 2, 'DALAM', '', 3, '', '2020-07-07 16:22:34', 'b02bfaf862f885b2a68834c99dff03418a4f921497fcc9b377bf3b88d5a324476a95c2381800595cdc61cba1cfa770c53b286985446e0fac5208d9b988386e0e', '860', 'ACTIVE', '::1'),
(3, 3, 'JOJON', '08562020610', 'ASD ASDASDAS DASD\r\nASDASD', 'M', 1, 'LUAR', '', 0, '', '2020-07-07 16:27:00', '4a61e28c55bc4ae29b3bf21413c30fd5892727b1ee07df0b58a34a62bd34edc91068b890b2225f0698d521cacb49c9c8db1e3e83dff85c27a070ab54ac045232', '0', 'ACTIVE', '::1'),
(4, 1, 'GERARDO', '08562020610', 'KOPO LESTARI B-13\r\nBANDUNG', 'M', 1, 'LUAR', '', 0, '', '2020-07-07 16:45:51', 'a631df3c16760b15cb7e71e1c845f66f65df9552103e0a71735e4dd0b6cea7dc2b5db37de744799e63c846af58809d7ef2d84e0ff3372ce555bc5b1c5c2deb59', '2a225975619660b232f8f058597197069d59f84331e9906e61', 'ACTIVE', '::1'),
(5, 2, 'ASDASD', 'ASDASD', 'ASD', 'M', 2, 'DALAM', '', 19, '', '2020-07-07 17:20:40', '4ca318e2087988e7f60d9956938d31dc15fb9e103397114353f072845fcf3b20a70ecc1b771e767ee3fa5a1661c546cdd74c3a08005f43fd271d82dda5b8191e', 'ccb65a9e54f8508bcd4490c14b55d0c76cbca8dc39341cc655', 'ACTIVE', '::1'),
(6, 2, 'ASDASD', 'ASDASD', 'ASD', 'M', 2, 'DALAM', '', 19, '', '2020-07-07 17:21:02', 'fd5b9aec801e41f77fce4b4acb1718d7fb7835a1df736956bb5b79f337c16e104c6b74bbdd742c11f05f2094d2a2f55bf297366a9b4a31e4a2ae5141f5fabd67', 'e6b4d757b7520a77c4ba7be3c95b76f87938f7b0343dd2d07d', 'ACTIVE', '::1'),
(7, 2, 'ASDASD', 'ASDASD', 'ASD', 'M', 2, 'DALAM', '', 19, '', '2020-07-07 17:21:14', '45d10148e7a7c42fc4dd97c4a49f78d3d7e3481b6b9c79ccf08234c47106e7b1e85f2c60bd2dcab6878b7b89bb3d18cdd296896dae1f31b695106e0d5fb5558a', 'd023806739e74c1f911ceaa2b3fb6e6d870819548bd9f99510', 'ACTIVE', '::1'),
(8, 2, 'ASDASD', 'ASDASD', 'ASD', 'M', 2, 'DALAM', '', 19, '', '2020-07-07 17:21:32', 'ec7a443aea1ebc4262bafc1a85e2706188549e70af97dfe3d4dba1da17e7ce4260bed3bbd17d71bbf1ca0b8d3ed8dfc2efe5fc77165b7ed708bb5db35c2dc439', '047ecf574a55161364a60d9c3b1a2bc031d16769794fc0002e', 'ACTIVE', '::1'),
(9, 1, 'ADASDASDASD', 'ASDASDSA', 'DASDASD', 'M', 1, 'LUAR', '', 0, 'AGUNG', '2020-07-07 17:27:48', 'e362d5ad086d349a952307d8ec8674034acad44ce119820bc1614be5043ac3a018673cf3c9315c80255d07e1512e758e9ffbf70714e2beaadf559cbd2d2f2783', '67ff7725eeb1ec69d51ab92de0f36eb9cf6e8f128e48020dba', 'ACTIVE', '::1'),
(10, 1, 'AAAA', '123123', 'BBBB', 'F', 4, '', '', 0, 'AGUNG', '2020-07-07 17:29:30', '9c841f507cd12c5173ccf039a7d17f76037335f0be241a322bc1e632d271b00c4e37d6884386b57a767c569634defd45241d05d13a3cd52063de7228df7aa36d', '2acb3731914d2c2043311600f3652cb8b7ee68e28333f6771f', 'ACTIVE', '::1'),
(11, 1, 'VVVVV', '123123', 'BBBBB', 'F', 2, 'LUAR', 'JABABEKA', 0, 'AGENG', '2020-07-07 17:30:22', '3bc6ae07fbff29c4e3e220c01fcb1946b9a0b7e8a6ace6f5a9676cd5fd73b958b85d2e9c9999291837b9a4c2b081a20df30a30313eaa68abc3b423e5baf175b3', 'e5e3dbbb1af8a7541089ddea772bc1df2fd08040b9ec73b7ea', 'ACTIVE', '::1'),
(12, 1, 'AAAA', 'DSD', 'SDA', 'M', 2, 'DALAM', '', 12, '', '2020-07-07 17:31:21', 'e92701e573fed5fdf3151d79c58c406065c853b176b8b1bbbfbddae798060b5fbec0e369c805030713776146046ef3d963f6bf352f032f5a8dd4a447ee97f1de', '52fa06baa1761ef0f11a6b02aacaaed8b742dbc915ff4aa6d4', 'ACTIVE', '::1'),
(13, 1, 'AAAA', 'DSD', 'SDA', 'M', 2, 'DALAM', '', 12, '', '2020-07-07 17:32:08', '0fd7a96709ed971a925404664adc4000b9f6d0b8d79016e8351896ff70b82650bca29c619e6b00a942c5f143a1ac49fa11c79f65434bf35a8095ce1e0eeeb78e', '37a2be3e22ba8b5ca2b37488de6cca52216f03f65037e78028', 'ACTIVE', '::1'),
(14, 1, 'ASD', 'ASD', 'ASD', 'M', 2, 'DALAM', '', 11, '', '2020-07-07 17:44:14', '52f4e3402c58008a666f8a396aa3e15b8da345992edf22171ca34d2b0d447be01130d1ade63cf7cb9f407f199f334f15d8ef5675752184f8461d1463bd79425c', 'd982f619d762acbf76fbb40d4bc60b0b856de272274e093038', 'ACTIVE', '::1'),
(15, 1, 'ASDASDASD', '2312312', 'ASDASD', 'M', 2, 'DALAM', '', 19, '', '2020-07-08 09:06:22', '40b8d47253590597a0584e06c90554fd1b03590d56a804da868023380a432e3397ffdb454864d25ff9ae0cc60fe32462fbb3a5e882d3d8f8d1a1d5fb9395c914', '016daeb01b8c37705d28385a1b2e5fc7fdc207f668672c6b29', 'ACTIVE', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `nama` varchar(160) NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `passkey` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`nama`, `kapasitas`, `passkey`) VALUES
('Gereja Katedral St. Petrus Bandung', 200, '');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `jadwal_id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`jadwal_id`, `nama`, `tanggal`, `status`, `date_added`) VALUES
(1, 'Sabtu, 11 Juli 2020 - 17.00', '2020-07-01', 1, '2020-07-07 07:27:54'),
(2, 'Minggu, 12 Juli 2020 - 06.00', '2020-07-12', 1, '2020-07-07 07:27:54'),
(3, 'Minggu, 12 Juli 2020 - 17.00', '2020-07-12', 1, '2020-07-07 07:28:07'),
(4, 'Sabtu, 18 Juli 2020 - 17.00', '2020-07-18', 1, '2020-07-07 16:32:15');

-- --------------------------------------------------------

--
-- Table structure for table `lingkungan`
--

CREATE TABLE `lingkungan` (
  `lingkungan_id` tinyint(4) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lingkungan`
--

INSERT INTO `lingkungan` (`lingkungan_id`, `nama`, `status`, `date_added`) VALUES
(1, 'Lingkungan Kerubim', 1, '2020-07-07 07:09:51'),
(2, 'Lingkungan Mikael', 1, '2020-07-07 07:09:51'),
(3, 'Lingkungan Rafael', 1, '2020-07-07 07:10:05'),
(4, 'Lingkungan Serafim', 1, '2020-07-07 07:10:05'),
(5, 'Lingkungan Borromeus', 1, '2020-07-07 07:10:14'),
(6, 'Lingkungan Carolus', 1, '2020-07-07 07:10:14'),
(7, 'Lingkungan Gabriel', 1, '2020-07-07 07:10:25'),
(8, 'Lingkungan Gerardus', 1, '2020-07-07 07:10:25'),
(9, 'Lingkungan Antonius', 1, '2020-07-07 07:10:38'),
(10, 'Lingkungan Ignatius', 1, '2020-07-07 07:10:38'),
(11, 'Lingkungan Theresia', 1, '2020-07-07 07:10:49'),
(12, 'Lingkungan Yosep', 1, '2020-07-07 07:10:49'),
(13, 'Lingkungan Agustinus', 1, '2020-07-07 07:10:59'),
(14, 'Lingkungan Elisabeth', 1, '2020-07-07 07:10:59'),
(15, 'Lingkungan Maria', 1, '2020-07-07 07:11:10'),
(16, 'Lingkungan Kristoforus', 1, '2020-07-07 07:11:10'),
(17, 'Lingkungan Martinus', 1, '2020-07-07 07:11:20'),
(18, 'Lingkungan Monica', 1, '2020-07-07 07:11:20'),
(19, 'Lingkungan Stefanus', 1, '2020-07-07 07:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `usia`
--

CREATE TABLE `usia` (
  `usia_id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usia`
--

INSERT INTO `usia` (`usia_id`, `nama`, `status`, `date_added`) VALUES
(1, '13-17 tahun', 1, '2020-07-07 08:52:47'),
(2, '18-30 tahun', 1, '2020-07-07 08:52:47'),
(3, '31-50 tahun', 1, '2020-07-07 08:53:03'),
(4, '51-60 tahun', 1, '2020-07-07 08:53:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkin`
--
ALTER TABLE `checkin`
  ADD PRIMARY KEY (`checkin_id`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`jadwal_id`);

--
-- Indexes for table `lingkungan`
--
ALTER TABLE `lingkungan`
  ADD PRIMARY KEY (`lingkungan_id`);

--
-- Indexes for table `usia`
--
ALTER TABLE `usia`
  ADD PRIMARY KEY (`usia_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkin`
--
ALTER TABLE `checkin`
  MODIFY `checkin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `jadwal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lingkungan`
--
ALTER TABLE `lingkungan`
  MODIFY `lingkungan_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `usia`
--
ALTER TABLE `usia`
  MODIFY `usia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
