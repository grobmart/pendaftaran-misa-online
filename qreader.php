<?php
require("config.php");
$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

//ga di design ya...
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Eldwen - Byxel.net">
        <title>Absensi Misa</title>
        <!-- Bootstrap core CSS -->
        <link href="assets/dist/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="form-validation.css" rel="stylesheet">
    </head>
    <body class="bg-light p-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <form method='post' action=''>
                        <input type='password' name='passkey' value='' class="form-control" placeholder='Masukkan Pass-Key Disini' />
                        <input type='hidden' name='code' value='<?php echo $_GET['token']; ?>' />
                        <button class="btn btn-primary btn-lg btn-block" type="submit" name="submit">CHECK</button>
                    </form>
                    <?php
                    if (isset($_POST['code']) && isset($_POST['passkey'])) {
                        //get config for name and passkey
                        $getConfig = mysqli_query($conn, "SELECT * FROM configs");
                        $config = mysqli_fetch_assoc($getConfig);
                        //end get config for name and passkey
                        
                        //check passkey betul atau salah
                        if (strtoupper($config['passkey']) == strtoupper($_POST['passkey'])) {
                            $code = mysqli_real_escape_string($conn, $_POST['code']);

                            //check DB apakah code dia ada dan belum dipakai
                            $checkCode = mysqli_query($conn, "SELECT COUNT(checkin_id) as total, status FROM checkin WHERE code = '" . $code . "'");
                            $getCode = mysqli_fetch_assoc($checkCode);
                            
                            if ($getCode['total'] > 0 && $getCode['status'] == "ACTIVE") {
                                //get detail data
                                $getData = mysqli_query($conn, "SELECT * FROM checkin WHERE code = '" . $code . "'");
                                $data = mysqli_fetch_assoc($getData);

                                //update ke DB status dia jadi checkin
                                mysqli_query($conn, "UPDATE checkin SET status='CHECKED_IN' WHERE code = '".$code."'");
                                
                                //notif sukses
                                echo '<center>';
                                echo '<h3>Sukses, Silahkan Masuk Sdr. ' . $data['nama'] . '</h3>';
                                echo '</center>';
                            } else if ($getCode['status'] == "CHECKED_IN") {
                                echo '<center>';
                                echo '<h3>Mohon Maaf QR Code Anda telah terpakai.</h3>';
                                echo '</center>';
                            } else if ($getCode['status'] == "BLOCKED") {
                                echo '<center>';
                                echo '<h3>Mohon Maaf Code Anda ter-blacklist oleh sistem. Hubungi petugas terkait</h3>';
                                echo '</center>';
                            } else{
                                echo '<center>';
                                echo '<h3>Mohon Maaf Kode Tidak Ditemukan Dalam Sistem Atau Telah Expired</h3>';
                                echo '</center>';
                            }
                        } else {
                            echo '<center>';
                            echo '<h3>Mohon Maaf Pass-Key Salah</h3>';
                            echo '</center>';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>